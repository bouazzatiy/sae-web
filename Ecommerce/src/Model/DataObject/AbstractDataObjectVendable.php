<?php

namespace Ecommerce\src\Model\DataObject;

abstract class AbstractDataObjectVendable extends AbstractDataObject
{
    public abstract function formatTableau(): array; 

    public abstract function getId(): string;
    public abstract function getNom(): string; 
    public abstract function getDescription(): string;
    public abstract function getPrix(): int;
    public abstract function getImage(): string;
      

    public abstract function setId(string $id);
    public abstract function setNom(string $nom);
    public abstract function setDescription(string $description);
    public abstract function setPrix(int $prix);
    public abstract function setImage(string $image);

}    