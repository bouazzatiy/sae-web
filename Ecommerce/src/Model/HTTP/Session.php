<?php
namespace Ecommerce\src\Model\HTTP;
    
use Exception;

class Session
{
    private static ?Session $instance = null;

    private function __construct()
    {
        if (session_start() === false) {
            throw new Exception("La session n'a pas réussi à démarrer.");
        }
    }

    public static function getInstance(): Session
    {
        if (is_null(static::$instance))
            static::$instance = new Session();
        return static::$instance;
    }

    public function contient($nom): bool
    {
        return isset($_SESSION[$nom]);
    }

    public function enregistrer(string $nom, mixed $valeur): void
    {
        $_SESSION[$nom] = $valeur;
    }
        
    public function lire(string $nom): mixed
    {
        return $_SESSION[$nom];
    }
        
    public function supprimer($nom): void
    {
        unset($_SESSION[$nom]);
    }
        
    public function detruire() : void
    {
        session_unset();
        session_destroy();
        Cookie::supprimer(session_name());
        $instance = null;
    }        
}






/*
namespace Ecommerce\src\Model\HTTP;
    
use Exception;

class Session
{
    private static ?Session $instance = null;

    
    private function __construct()
    {
        if (session_start() === false) {
            throw new Exception("La session n'a pas réussi à démarrer.");
        }
    }

    public static function getInstance(): Session
    {
        if (is_null(static::$instance))
            static::$instance = new Session();
        return static::$instance;
    }

    public function contient($name) : bool
    {
        return isset($_SESSION[$name]);
    }

    public function enregistrer(string $name, mixed $value): void
    {
        if (isset($_SESSION[$name][$value])) {
            $_SESSION['panier'][$value] ++;
        } else {
            $_SESSION['panier'][$value] = 1;
        }
    }
        
    public function lire(string $name): mixed
    {
        return $_SESSION[$name];
    }
        
    public function supprimer($name): void
    {
        unset($_SESSION[$name]);
    }
        
    public function detruire() : void
    {
        session_unset();     // unset $_SESSION variable for the run-time
        session_destroy();   // destroy session data in storage
        Cookie::supprimer(session_name()); // deletes the session cookie
        // Il faudra reconstruire la session au prochain appel de getInstance()
        $instance = null;
    }        
}*/