<div>
    <h2>Connexion</h2>

    <form method="get">


        <div class="centreurBouton">

            <div class="contenuForm">
                <p>
                    <label for="email_id">Adresse mail</label>
                    <input type="email" placeholder="bob@feuille.com" name="email" id="email_id" required/>
                </p>
                <p>
                    <label for="password_id">Mot de passe</label>
                    <input type="password" placeholder="*******" name="password" id="password_id" required/>
                </p>
            </div>

        </div>




        <div class="centreurBouton">
            <input type="submit" value="Se connecter" class="boutonStyleLBR"/>
            <input type='hidden' name='action' value='connecter'>
            <input type='hidden' name='controller' value='client'>
        </div>

    </form>

    <p>
        Vous n'êtes pas membre ? <a class="lienSimple" href="?controller=Client&action=register">S'inscrire maintenant !</a>
    </p>

</div>