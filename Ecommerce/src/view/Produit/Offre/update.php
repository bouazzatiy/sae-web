<?php
    /** @var Offre  $offre*/
?>

<div>
<form method="get" >
    <fieldset>
        <legend>Modifier la souffleur:</legend>
        <p>
            <label for="id_id">Identifiant</label> :
            <input type="text" value=<?= htmlspecialchars($offre->getId()) ?> name="idProduit" id="id_id" readonly/>
        </p>
        <p>
            <label for="nom_id">Nom</label> :
            <input type="text" value=<?= htmlspecialchars($offre->getNom()) ?> name="nomProduit" id="nom_id" />
        </p>
        <p>
            <label for="prix_id">Prix</label> :
            <input type="number" value=<?= htmlspecialchars($offre->getPrix()) ?> name="prixProduit" id="prix_id"/>
        </p>    
        <p>
            <label for="description_id">Description</label> :
            <textarea name="descriptionProduit" id="description_id" rows="4" cols="50" required><?= htmlspecialchars($offre->getDescription()) ?></textarea>
        </p>
        <p>
            <label for="image_id">Image</label> :
            <input type="file" name="imageProduit" id="image_id" accept="image/png, image/jpeg" />
        </p>
        
        <p>
            <input type="submit" value="Envoyer" />
            <input type='hidden' name='action' value='updated'>
            <input type='hidden' name='controller' value='souffleur'>
        </p>
    </fieldset>
</form>

</div>