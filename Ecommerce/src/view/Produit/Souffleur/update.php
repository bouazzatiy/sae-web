<?php
    /** @var Souffleur  $souffleur*/
?>

<div>

    <h2>Modifier le souffleur</h2>

<form method="get" >
        <p>
            <label for="id_id">Identifiant</label>
            <input type="text" value=<?= htmlspecialchars($souffleur->getId()) ?> name="idProduit" id="id_id" readonly/>
        </p>
        <p>
            <label for="nom_id">Nom</label>
            <input type="text" value=<?= htmlspecialchars($souffleur->getNom()) ?> name="nomProduit" id="nom_id" />
        </p>
        <p>
            <label for="prix_id">Prix</label>
            <input type="number" value=<?= htmlspecialchars($souffleur->getPrix()) ?> name="prixProduit" id="prix_id"/>
        </p>    
        <p>
            <label for="description_id">Description</label>
            <textarea name="descriptionProduit" id="description_id" rows="4" cols="50" required><?= htmlspecialchars($souffleur->getDescription()) ?></textarea>
        </p>
        <p>
            <label for="image_id">Image</label>
            <input type="file" name="imageProduit" id="image_id" accept="image/png, image/jpeg" />
        </p>
        <p>
            <label for="puissance_id">Puissance</label>
            <input type="number" value=<?= htmlspecialchars($souffleur->getPuissance()) ?> name="puissance" id="puissance_id"/>
        </p>
        <p>
            <label for="consommation_id">Consommation électrique</label>
            <input type="number" value=<?= htmlspecialchars($souffleur->getConsommationElectrique()) ?> name="consommation" id="consommation_id"/>
        </p>
        
        <p>
            <input type="submit" value="Envoyer" class="boutonStyleLBR"/>
            <input type='hidden' name='action' value='updated'>
            <input type='hidden' name='controller' value='souffleur'>
        </p>
</form>

</div>