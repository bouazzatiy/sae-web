<?php

namespace Ecommerce\src\Model\Repository;

use Ecommerce\src\Model\DataObject\Client;
use PDOException;

class ClientRepository extends AbstractRepository
{
    protected function construire(array $clientFormatTableau): Client
    {
        return new Client(
            $clientFormatTableau['idClient'],
            $clientFormatTableau['nomClient'],
            $clientFormatTableau['prenomClient'],
            $clientFormatTableau['emailClient'],
            $clientFormatTableau['motDePasseClient'],
            $clientFormatTableau['admin']
        );
    }

    protected function getNomTable(): string
    {
        return 'p_clients';
    }

    protected function getNomClePrimaire(): string
    {
        return 'idClient';
    }

    protected function getNomsColonnes(): array
    {
        return array(
            'idClient',
            'nomClient',
            'prenomClient',
            'emailClient',
            'motDePasseClient',
            'admin'
        );
    }
    public function updateWithoutPassword(Client $client)
    {
        $sql = 'UPDATE '.$this->getNomTable().' SET ';
        foreach ($this->getNomsColonnes() as $nomColonne) 
        {
            if ($nomColonne != 'motDePasseClient')
            {
                $sql = $sql.$nomColonne.' = :'.$nomColonne.', ';
            }
        }
        $sql = substr($sql, 0, -2).' WHERE '.$this->getNomClePrimaire().' = :'.$this->getNomClePrimaire();
        $pdoStatement = DatabaseConnection::getPdo()->prepare($sql);
  
        try{
            $pdoStatement->execute($client->formatTableau());
        }catch(PDOException $e){
            return false;
        }
    }

    public function insertWithoutId(Client $objet) : bool
    {
        $sql = 'INSERT INTO '.$this->getNomTable().' (';
        
        foreach ($this->getNomsColonnes() as $nomColonne) 
        {
            if($nomColonne != 'idClient')
            {
                $sql = $sql.$nomColonne.', ';
            }
        }
        
        $sql = substr($sql, 0, -2).')';
        $sql = $sql.' VALUES (';
        
        foreach($this->getNomsColonnes() as $nomColonne)
        {
           if($nomColonne != 'idClient')
            {
                $sql = $sql.':'.$nomColonne.', ';
            }
        }
        $sql = substr($sql, 0, -2).')';

        $pdoStatement = DatabaseConnection::getPdo()->prepare($sql);
        
        try{
            $pdoStatement->execute($objet->formatTableauWithoutId());
        }catch(PDOException $e){
            echo $sql;
            echo json_encode($objet->formatTableauWithoutId());
            echo $e->getMessage();
            return false;
        }
        return true;
    }

    public function selectByEmail(string $email) : ?Client
    {
        $sql = 'SELECT * FROM p_clients WHERE emailClient = :emailClient';
        $pdoStatement = DatabaseConnection::getPdo()->prepare($sql);
        $values = array(
            'emailClient' => $email
        );
        $pdoStatement->execute($values);
        $objet = $pdoStatement->fetch();
        if ($objet)
            return $this->construire($objet);
        else
            return null;
    }
}