<?php

namespace Ecommerce\src\Controller;

use Ecommerce\src\Lib\MessageFlash;

abstract class AbstractController
{
    public abstract static function default() : void;

    protected static function afficheVue(string $cheminVue, array $parametres = []) : void {

        extract($parametres); // Crée des variables à partir du tableau $parametres
        $messagesFlash = MessageFlash::lireTousMessages();
        $types = MessageFlash::$types;
        require __DIR__ . "/../view/$cheminVue"; // Charge la vue

    }
}