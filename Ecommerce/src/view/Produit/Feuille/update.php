<?php
    /** @var Feuille $feuille*/
?>

<div>

    <h2>Modifier la feuille</h2>
<form method="get" >
        <p>
            <label for="id_id">Identifiant</label>
            <input type="text" value=<?= htmlspecialchars($feuille->getId()) ?> name="idProduit" id="id_id" readonly/>
        </p>
        <p>
            <label for="nom_id">Nom</label>
            <input type="text" value=<?= htmlspecialchars($feuille->getNom()) ?> name="nomProduit" id="nom_id" />
        </p>
        <p>
            <label for="prix_id">Prix</label>
            <input type="number" value=<?= htmlspecialchars($feuille->getPrix()) ?> name="prixProduit" id="prix_id"/>
        </p>    
        <p>
            <label for="description_id">Description</label>
            <textarea name="descriptionProduit" id="description_id" rows="4" cols="50" required><?= htmlspecialchars($feuille->getDescription()) ?></textarea>
        </p>
        <p>
            <label for="image_id">Image</label>
            <input type="file" name="imageProduit" id="image_id" accept="image/png, image/jpeg" />
        </p>
        <p>
            <label for="masse_id">Masse</label>
            <input type="number" value=<?= htmlspecialchars($feuille->getMasse()) ?> name="masse" id="masse_id"/>
        </p>
        
        <p>
            <input type="submit" value="Envoyer" class="boutonStyleLBR"/>
            <input type='hidden' name='action' value='updated'>
            <input type='hidden' name='controller' value='Feuille'>
        </p>
</form>
</div>
