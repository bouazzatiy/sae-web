<?php

namespace Ecommerce\src\Model\Repository;

use Ecommerce\src\Model\DataObject\Feuille;
use PDOException;

class FeuilleRepository extends AbstractRepository
{
    protected function construire(array $feuilleFormatTableau): Feuille
    {
        return new Feuille(
            $feuilleFormatTableau['idProduit'],
            $feuilleFormatTableau['nomProduit'],
            $feuilleFormatTableau['descriptionProduit'],
            $feuilleFormatTableau['prixProduit'],
            $feuilleFormatTableau['imageProduit'],
            $feuilleFormatTableau['masse']
        );
    }

    protected function getNomTable(): string
    {
        return 'p_feuilles';
    }

    protected function getNomClePrimaire(): string
    {
        return 'idFeuille';
    }

    protected function getNomsColonnes(): array
    {
        return array(
            'idProduit',
            'nomProduit',
            'descriptionProduit',
            'prixProduit',
            'imageProduit',
            'masse'
        );
    }

    public function insertWithoutId(Feuille $objet) : bool
    {
        $sql = "INSERT INTO p_produits (nomProduit, descriptionProduit, prixProduit, imageProduit) VALUES (:nomProduit, :descriptionProduit, :prixProduit, :imageProduit);
                INSERT INTO p_feuilles (idFeuille, masse) VALUES (LAST_INSERT_ID(), :masse);";

        
        $pdoStatement = DatabaseConnection::getPdo()->prepare($sql);
        
        try{
            $pdoStatement->execute($objet->formatTableauWithoutId());
        }catch(PDOException $e){
            return false;
        }
        return true;
    }

    public function selectFeuille(string $valeurClePrimaire): Feuille
    {
        $sql = 'SELECT idProduit, nomProduit, descriptionProduit, prixProduit, imageProduit, masse FROM p_feuilles JOIN p_produits ON idFeuille = idProduit WHERE idFeuille = :valeurClePrimaire';
        $pdoStatement = DatabaseConnection::getPdo()->prepare($sql);
        $values = array(
            "valeurClePrimaire" => $valeurClePrimaire
        );
        $pdoStatement->execute($values);

        $objet = $pdoStatement->fetch();
        if ($objet)
            return $this->construire($objet);
        else
            return null;
    }
    

    public function selectAllFeuille(): array
    {
        $sql = 'SELECT idProduit, nomProduit, descriptionProduit, prixProduit, imageProduit, masse FROM p_feuilles JOIN p_produits ON idFeuille = idProduit';
        $pdoStatement = DatabaseConnection::getPdo()->prepare($sql);
        
        $pdoStatement->execute();

        $objets = array();
        foreach ($pdoStatement as $objetFormatTableau)
        {
            $objets[] = $this->construire($objetFormatTableau);
        }
        return $objets;
    }

    public function updateFeuille(Feuille $feuille): bool
    {
        $sql = 
            'UPDATE p_feuilles 
             SET masse = :masse
             WHERE idFeuille = :idProduit;
             UPDATE p_produits
             SET nomProduit = :nomProduit, descriptionProduit = :descriptionProduit, prixProduit = :prixProduit
             WHERE idProduit = :idProduit';
        $pdoStatement = DatabaseConnection::getPdo()->prepare($sql);
        try{
            $pdoStatement->execute($feuille->formatTableau());;
        }catch(PDOException $e){
            return false;
        }
        return true;
    }
}