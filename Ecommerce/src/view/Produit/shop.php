<?php
/** @var Produit $produit*/

use Ecommerce\src\Model\DataObject\Produit;



echo "<div>
        <h2>Liste de feuilles</h2>
        <div class='liste'>
";

foreach ($produits as $produit) {

    $produitHTML=htmlspecialchars($produit->getNom());
    $prixHTML=htmlspecialchars($produit->getPrix());
    $idProduitHTML= htmlspecialchars($produit->getId());
    $srcImage= htmlspecialchars($produit->getImage());
    echo "        
        
        <div class='vitrineFeuille'>
            <a class='lienSimple' href=\"?controller=Produit&action=buy&idProduit={$idProduitHTML}\">
                   <img class=\"image_feuille\" src=\"image/$srcImage\"alt=\"{$produitHTML}\">
                   <p>{$produitHTML}  {$prixHTML}€</p> 
            </a>
        </div>
                
                
                ";
}


echo "</div></div>";

?>