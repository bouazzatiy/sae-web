
<form method="get" >

    <fieldset>
        <legend>Formulaire client:</legend>
        <p>
            <label for="nom_id">Nom</label> :
            <input type="text" placeholder="Nom" name="nom" id="nom_id" required/>
        </p>
        <p>
            <label for="prenom_id">Prenom</label> :
            <input type="text" placeholder="Prenom" name="prenom" id="prenom_id" required/>
        </p>
        <p>
            <label for="email_id">Email</label> :
            <input type="email" placeholder="email@email.com" name="email" id="email_id" required/>
        </p>
        <p>
            <label for="password_id">Mot de passe</label> :
            <input type="password" placeholder="*******" name="password" id="password_id" required/>
        </p>
        <p>
            <input type="submit" value="Envoyer" />
            <input type='hidden' name='action' value='created'>
            <input type='hidden' name='controller' value='client'>
        </p>
    </fieldset>
</form>
