<?php

namespace Ecommerce\src\Model\Repository;

use Ecommerce\src\Model\DataObject\Produit;

class ProduitRepository extends AbstractRepository
{
    protected function construire(array $produitFormatTableau): Produit
    {
        return new Produit(
            $produitFormatTableau['idProduit'],
            $produitFormatTableau['nomProduit'],
            $produitFormatTableau['descriptionProduit'],
            $produitFormatTableau['prixProduit'],            
            $produitFormatTableau['imageProduit']
        );
    }

    protected function getNomTable(): string
    {
        return 'p_produits';
    }

    protected function getNomClePrimaire(): string
    {
        return 'idProduit';
    }

    protected function getNomsColonnes(): array
    {
        return array(
            'idProduit',
            'nomProduit',
            'descriptionProduit',
            'prixProduit',
            'imageProduit'
        );
    }
}