<?php

namespace Ecommerce\src\Model\Repository;
use Ecommerce\src\Model\DataObject\AbstractDataObject;
use PDOException;

abstract class AbstractRepository
{
    public function selectAll(): array
    {
        $pdoStatement = DatabaseConnection::getPdo()->query('SELECT * FROM '.$this->getNomTable());

        $objets = array();
        foreach ($pdoStatement as $objetFormatTableau)
        {
            $objets[] = $this->construire($objetFormatTableau);
        }
        return $objets;
    }

    public function select(string $valeurClePrimaire): ?AbstractDataObject
    {
        $sql = 'SELECT * FROM '.$this->getNomTable().' WHERE '.$this->getNomClePrimaire().' = :valeurClePrimaire';
        $pdoStatement = DatabaseConnection::getPdo()->prepare($sql);
        $values = array(
            "valeurClePrimaire" => $valeurClePrimaire
        );
        $pdoStatement->execute($values);

        $objet = $pdoStatement->fetch();
        if ($objet)
            return $this->construire($objet);
        else
            return null;
    }

    public function delete(string $valeurClePrimaire) : bool
    {
        $sql = 'DELETE FROM '.$this->getNomTable().' WHERE '.$this->getNomClePrimaire().' = :valeurClePrimaire';
        $pdoStatement = DatabaseConnection::getPdo()->prepare($sql);

        $values = array(
            "valeurClePrimaire" => $valeurClePrimaire
        );
        
        try{
            $pdoStatement->execute($values);
        }catch(PDOException $e){
            return false;
        }
        return true;
    }

    public function update(AbstractDataObject $objet)
    {
        $sql = 'UPDATE '.$this->getNomTable().' SET ';
        foreach ($this->getNomsColonnes() as $nomColonne) 
        {
            $sql = $sql.$nomColonne.' = :'.$nomColonne.', ';
        }
        $sql = substr($sql, 0, -2).' WHERE '.$this->getNomClePrimaire().' = :'.$this->getNomClePrimaire();
        $pdoStatement = DatabaseConnection::getPdo()->prepare($sql);

        $pdoStatement->execute($objet->formatTableau());
    }

    public function insert(AbstractDataObject $objet) : bool
    {
        $sql = 'INSERT INTO '.$this->getNomTable().' VALUES (';
        foreach($this->getNomsColonnes() as $nomColonne)
        {
            $sql = $sql.':'.$nomColonne.', ';
        }
        $pdoStatement = DatabaseConnection::getPdo()->prepare($sql);
   
        try{
            $pdoStatement->execute($objet->formatTableau());
        }catch(PDOException $e){
            return false;
        }
        return true;

    }

    
    protected abstract function getNomTable(): string;
    protected abstract function construire(array $objetFormatTableau): AbstractDataObject;
    protected abstract function getNomClePrimaire(): string;
    protected abstract function getNomsColonnes(): array;
}