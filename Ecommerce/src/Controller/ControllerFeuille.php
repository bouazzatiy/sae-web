<?php
namespace Ecommerce\src\Controller;

use Ecommerce\src\Model\DataObject\Feuille;
use Ecommerce\src\Model\Repository\FeuilleRepository;

class ControllerFeuille extends AbstractController
{

    public static function default() : void {
        self::readAll();
    }

    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function readAll() : void {
        $feuilles = (new FeuilleRepository())->selectAll(); //appel au modèle pour gerer la BD
        //require __DIR__ . '/../view/Feuille/list.php';  //"redirige" vers la vue
        ControllerFeuille::afficheVue('../view/view.php',["feuilles"=>$feuilles,'pagetitle'=>"Liste des Feuilles", "cheminVueBody"=>"Produit/shop.php"]);
    }

    public static function  read() : void {
            $idFeuille =$_GET['idFeuille'];
            $Feuille = (new FeuilleRepository())->select($idFeuille);
            if(is_null($Feuille) ){
                ControllerFeuille::afficheVue('../view/view.php',['pagetitle'=>"Détails du Feuille", "cheminVueBody"=>"Produit/error.php"]);
            }else{
                $parametre =array($Feuille);
                ControllerFeuille::afficheVue('../view/view.php',['Feuille'=>$Feuille,'pagetitle'=>"Détails de la Feuille", "cheminVueBody"=>"Produit/detail.php"]);
            }
    }

    public static function buy() : void {
        ControllerFeuille::afficheVue('../view/view.php',['pagetitle'=>"Acheter une Feuille", "cheminVueBody"=>"Produit/buy.php"]);
    }

    public static function  create() : void {
        ControllerFeuille::afficheVue('../view/view.php',['pagetitle'=>"Créer une Feuille", "cheminVueBody"=>"Produit/create.php"]);
    }

    public static function created() : void {
        $Feuille = new Feuille(0,$_GET['nomProduit'],$_GET['descriptionProduit'],$_GET['prixProduit'],$_GET['imageProduit'],$_GET['masse']);
        $creer = (new FeuilleRepository)->insertWithoutId($Feuille);
        if ( $creer) ControllerFeuille::afficheVue('../view/view.php',['pagetitle'=>"Feuille créée", "cheminVueBody"=>"Produit/created.php"]);
        else{
            ControllerFeuille::afficheVue('../view/view.php',['pagetitle'=>"Feuille non créée", "cheminVueBody"=>"Produit/error.php"]);
        }
    }

    public static function error( string $errorMessage ="") {
        
        ControllerFeuille::afficheVue('../view/Feuille/error.php',['message'=>$errorMessage]);
    }
    public static function delete() : void {
        ControllerFeuille::afficheVue('../view/view.php',['pagetitle'=>"Créer une Feuille", "cheminVueBody"=>"Produit/delete.php"]);
    }

    public static function deleted() : void {
        $idFeuille=$_GET['idFeuille'];
        $supprimer= (new FeuilleRepository())->delete($idFeuille);
        if($supprimer){
            ControllerFeuille::afficheVue('../view/view.php',['pagetitle'=>"Feuille supprimée", "cheminVueBody"=>"Produit/deleted.php", 'idFeuille'=>$idFeuille]);
        }else{
            ControllerFeuille::afficheVue('../view/view.php',['pagetitle'=>"Feuille non supprimée", "cheminVueBody"=>"Produit/error.php", "message"=>'Feuille non supprimée']);
        }
    }

    public static function update() : void {
        $idFeuille=$_GET['idproduit'];
        $Feuille=(new FeuilleRepository())->selectFeuille($idFeuille);
        if($Feuille != null){

            ControllerFeuille::afficheVue('../view/view.php',['pagetitle'=>"Modifier un Feuille", "cheminVueBody"=>"Produit/Feuille/update.php", 'feuille'=>$Feuille]);
        }
        else{
            ControllerFeuille::afficheVue('../view/view.php',['pagetitle'=>"Feuille non supprimé", "cheminVueBody"=>"Produit/error.php"]);
        }

    }

    public static function updated() : void {
        $Feuille = new Feuille($_GET['idProduit'],$_GET['nomProduit'],$_GET['descriptionProduit'],$_GET['prixProduit'],$_GET['imageProduit'],$_GET['masse']);
        
        $update=(new FeuilleRepository)->updateFeuille($Feuille);
        if($update){ControllerFeuille::afficheVue('../view/view.php',['pagetitle'=>"Feuille modifiée", "cheminVueBody"=>"Produit/updated.php",'id'=>$_GET['idProduit']]);}
        else{
            ControllerFeuille::afficheVue('../view/view.php',['pagetitle'=>"Feuille non modifiée", "cheminVueBody"=>"Produit/error.php"]);
        }
    }
}
?>











