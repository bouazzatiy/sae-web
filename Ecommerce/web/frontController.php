<?php

use Ecommerce\src\Controller\ControllerClient;
use Ecommerce\src\Controller\ControllerProduit;


require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';

// instantiate the loader
$loader = new Ecommerce\src\Lib\Psr4AutoloaderClass();
// register the base directories for the namespace prefix
$loader->addNamespace('Ecommerce\src', __DIR__ . '/../src');
// register the autoloader
$loader->register();

// On recupère le controller passé dans l'URL

//error_reporting(0);

    if (!isset($_GET['controller'])){
        $controller = 'accueil';
    }else {
        $controller = $_GET['controller'];
    }
    $controllerClassName='Ecommerce\src\Controller\Controller'.ucfirst($controller);

    if(class_exists($controllerClassName)){
        // On recupère l'action passée dans l'URL
        if(!isset($_GET['action'])){
            $action='default';
        }
        else{
            $action = $_GET['action'];
        }
        $methodClass= get_class_methods($controllerClassName);

        if (in_array($action, $methodClass)) {
            $controllerClassName::$action();
        }else{
            $controllerClassName::error("Cette action n'existe pas");
        }
    }else{
        if($controller==''){
            ControllerProduit::readAll();
        }else{
            ControllerClient::error("Le controller entré en paramètre n'existe pas");
        }
    }




?>


