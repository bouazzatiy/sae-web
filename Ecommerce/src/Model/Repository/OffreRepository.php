<?php

namespace Ecommerce\src\Model\Repository;

use Ecommerce\src\Model\DataObject\Offre;
use PDOException;

class OffreRepository extends AbstractRepository
{
    protected function construire(array $feuilleFormatTableau): Offre
    {
        return new Offre(
            $feuilleFormatTableau['idProduit'],
            $feuilleFormatTableau['nomProduit'],
            $feuilleFormatTableau['descriptionProduit'],
            $feuilleFormatTableau['prixProduit'],            
            $feuilleFormatTableau['imageProduit']
        );
    }

    protected function getNomTable(): string
    {
        return 'p_vueOffres';
    }

    protected function getNomClePrimaire(): string
    {
        return 'idProduit';
    }

    protected function getNomsColonnes(): array
    {
        return array(
            'idProduit',
            'nomProduit',
            'descriptionProduit',
            'prixProduit',
            'imageProduit'
        );
    }
    public function selectAllOffre(): array
    {
        $sql = 'SELECT idProduit, nomProduit, descriptionProduit, prixProduit, imageProduit FROM p_offres JOIN p_produits ON idOffre = idProduit';
        $pdoStatement = DatabaseConnection::getPdo()->prepare($sql);
        
        $pdoStatement->execute();

        $objets = array();
        foreach ($pdoStatement as $objetFormatTableau)
        {
            $objets[] = $this->construire($objetFormatTableau);
        }
        return $objets;
    }

    public function insertWithoutId(Offre $objet) : bool
    {
        $sql = "INSERT INTO p_produits (nomProduit, descriptionProduit, prixProduit, imageProduit) VALUES (:nomProduit, :descriptionProduit, :prixProduit, :imageProduit);   
                INSERT INTO p_offres (idOffre) VALUES (LAST_INSERT_ID());";

        
        $pdoStatement = DatabaseConnection::getPdo()->prepare($sql);
        
        try{
            $pdoStatement->execute($objet->formatTableauWithoutId());
        }catch(PDOException $e){
            return false;
        }
        return true;
    }

    public function selectOffre(string $valeurClePrimaire): Offre
    {
        $sql = 'SELECT idProduit, nomProduit, descriptionProduit, prixProduit, imageProduit
         FROM p_offres 
         JOIN p_produits ON idOffre = idProduit WHERE idOffre = :valeurClePrimaire';
        $pdoStatement = DatabaseConnection::getPdo()->prepare($sql);
        $values = array(
            "valeurClePrimaire" => $valeurClePrimaire
        );
        $pdoStatement->execute($values);

        $objet = $pdoStatement->fetch();
        if ($objet)
            return $this->construire($objet);
        else
            return null;
    }
   
   public function updateOffre(Offre $offre): bool
    {
        $sql = 
            '
             UPDATE p_produits
             SET nomProduit = :nomProduit, descriptionProduit = :descriptionProduit, prixProduit = :prixProduit
             WHERE idProduit = :idProduit';
        $pdoStatement = DatabaseConnection::getPdo()->prepare($sql);
        try{
            $pdoStatement->execute($offre->formatTableau());;
        }catch(PDOException $e){
            return false;
        }
        return true;
    }
}