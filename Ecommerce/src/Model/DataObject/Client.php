<?php

namespace Ecommerce\src\Model\DataObject;

class Client extends AbstractDataObject
{
    private string $id;
    private string $nom;
    private string $prenom;
    private string $email;
    private string $motDePasse;
    private bool $admin;

    public function __construct(string $id, string $nom, string $prenom, string $email, string $motDePasse, bool $admin)
    {
        $this->id = $id;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->email = $email;
        $this->motDePasse = $motDePasse;
        $this->admin = $admin;
    }

    public function formatTableau(): array
    {
        return array(
            'idClient' => $this->id,
            'nomClient' => $this->nom,
            'prenomClient' => $this->prenom,
            'emailClient' => $this->email,
            'motDePasseClient' => $this->motDePasse,
            'admin' => $this->admin
        );
    }
    public function formatTableauWithoutId(): array
    {
        return array(
            'nomClient' => $this->nom,
            'prenomClient' => $this->prenom,
            'emailClient' => $this->email,
            'motDePasseClient' => $this->motDePasse,
            'admin' => (int)$this->admin
        );
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getNom(): string
    {
        return $this->nom;
    }

    public function getPrenom(): string
    {
        return $this->prenom;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getMotDePasse(): string
    {
        return $this->motDePasse;
    }

    public function getAdmin(): bool
    {
        return $this->admin;
    }

    public function setId(string $id)
    {
        $this->id = $id;
    }

    public function setNom(string $nom)
    {
        $this->nom = $nom;
    }

    public function setPrenom(string $prenom)
    {
        $this->prenom = $prenom;
    }

    public function setEmail(string $email)
    {
       $this->email = $email;
    }

    public function setMotDePasse(string $motDePasse)
    {
        $this->motDePasse = $motDePasse;
    }

    public function setAdmin(bool $admin)
    {
        $this->admin = $admin;
    }
}