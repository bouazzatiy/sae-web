<?php

namespace Ecommerce\src\Model\Repository;

use Ecommerce\src\Model\DataObject\Panier;
use Ecommerce\src\Model\HTTP\Session;

class PanierRepository 
{
    public function __construct()
    {
        
    }

    public function insert(): bool
    {
        $sql = '
            INSERT INTO p_commandes(idCommande,prixTotal)
            VALUES (NULL,:prixTotal);
            INSERT INTO p_passer(idCommande,idClient, `date`) 
            VALUES (LAST_INSERT_ID(),:idClient, current_timestamp());
                ';

        $values = array(
            "prixTotal" => Session::getInstance()->lire("prixTotal"),
            "idClient" => 0,
        );
        $i = 0;
        foreach (Panier::lire() as $produit) {
            
            
            $i++;
            $sql .= '
            INSERT INTO p_contenir(idCommande,idProduit,quantite)
            VALUES (LAST_INSERT_ID(),:idProduit'.$i.',:quantite'.$i.');
                ';
            $pdoStatement = DatabaseConnection::getPdo()->prepare($sql);
            $values["idProduit$i"] = $produit[0]->getId();
            $values["quantite$i"] = $produit[1];

        }
        
        $pdoStatement = DatabaseConnection::getPdo()->prepare($sql);
        
        try {
            $pdoStatement->execute($values);
        } catch (\PDOException $e) {
            return false;
        }
        return true;

    }
}