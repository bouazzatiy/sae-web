<?php

namespace Ecommerce\src\Model\DataObject;

class Produit extends AbstractDataObjectVendable
{
    private string $id;
    private string $nom;
    private string $description;
    private int $prix;
    private string $image;

    public function __construct(int $id, string $nom, string $description, int $prix, string $image)
    {
        $this->id = $id;
        $this->nom = $nom;
        $this->description = $description;
        $this->prix = $prix;
        $this->image = $image;
    }

    public function formatTableau(): array
    {
        return array(
            'idProduit' => $this->id,
            'nomProduit' => $this->nom,
            'descriptionProduit' => $this->description,
            'prixProduit' => $this->prix,
            'imageProduit' => $this->image
        );
    }
    
    public function formatTableauWithoutId(): array
    {
        return array(
            'nomProduit' => $this->nom,
            'descriptionProduit' => $this->description,
            'prixProduit' => $this->prix,
            'imageProduit' => $this->image
        );
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getNom(): string
    {
        return $this->nom;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getPrix(): int
    {
        return $this->prix;
    }

    public function getImage(): string
    {
        return $this->image;
    }


    public function setId(string $id)
    {
        $this->id = $id;
    }

    public function setNom(string $nom)
    {
        $this->nom = $nom;
    }

    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    public function setPrix(int $prix)
    {
        $this->prix = $prix;
    }

    public function setImage(string $image)
    {
        $this->image = $image;
    }

    public function getTypeproduit(): string{
        return 'produit';
    }
}