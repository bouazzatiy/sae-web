
<div>
      <input type="radio" id="ajouterFeuille" name="produit" value="ajouterFeuille" checked>
      <label for="ajouterFeuille">Ajouter une feuille</label>

<form id="formulaireFeuille" method="get" >
    <fieldset>
        <p>
            <label for="nom_id">Nom</label> :
            <input type="text" placeholder="Nom" name="nomProduit" id="nom_id" required/>
        </p>
        <p>
            <label for="prix_id">Prix</label> :
            <input type="number" placeholder="10" name="prixProduit" id="prix_id" required/>
        </p>
        <p>
            <label for="image_id">Image</label> :
            <input type="file" name="imageProduit" id="image_id" accept="image/png, image/jpeg" />
        </p>
        <p>
            <label for="description_id">Description</label> :
            <textarea name="descriptionProduit" id="description_id" rows="4" cols="50" required>Veuillez entrer une description...</textarea>
        </p>
        <p>
            <label for="masse_id">Masse</label> :
            <input type="number" placeholder="1" name="masse" id="masse_id" required/>
        </p>
        <p>
            <input type="submit" value="Envoyer" />
            <input type='hidden' name='action' value='created'>
            <input type='hidden' name='controller' value='feuille'>
        </p>
    </fieldset>
</form>
</div>
<div>
      <input type="radio" id="ajouterOffre" name="produit" value="ajouterOffre" >
      <label for="ajouterOffre">Ajouter une offre</label>

<form id="formulaireOffre" method="get" >
    <fieldset>
    <p>
            <label for="nom_id">Nom</label> :
            <input type="text" placeholder="Nom" name="nom" id="nom_id" required/>
        </p>
        <p>
            <label for="prix_id">Prix</label> :
            <input type="number" placeholder="10" name="prix" id="prix_id" required/>
        </p>
        <p>
            <label for="image_id">Image</label> :
            <input type="file" name="image" id="image_id" accept="image/png, image/jpeg" />
        </p>
        <p>
            <label for="description_id">Description</label> :
            <textarea name="description" id="description_id" rows="4" cols="50" required>Veuillez entrer une description...</textarea>
        </p>
        <p>
            <label for="listeProduit_id">Produits</label> :
            <select name="produit" id="listeProduit_id" multiple >
                <?php
                    foreach ($listeProduit as $p) {
                        $nom = htmlspecialchars($p->getNom());
                        $id = htmlspecialchars($p->getId());
                        echo "<option value='$id'>$id - $nom</option>";
                    }?>
            </select>
        </p>

        <p>
            <input type="submit" value="Envoyer" />
            <input type='hidden' name='action' value='created'>
            <input type='hidden' name='controller' value='offre'>
        </p>
    </fieldset>
</form>
</div>
<div>
      <input type="radio" id="ajouterSouffleur" name="produit" value="ajouterSouffleur" >
      <label for="ajouterSouffleur">Ajouter un souffleur</label>

<form id="formulaireSouffleur" method="get" >
    <fieldset>
    <p>
            <label for="nom_id">Nom</label> :
            <input type="text" placeholder="Nom" name="nom" id="nom_id" required/>
        </p>
        <p>
            <label for="prix_id">Prix</label> :
            <input type="number" placeholder="10" name="prix" id="prix_id" required/>
        </p>
        <p>
            <label for="image_id">Image</label> :
            <input type="file" name="image" id="image_id" accept="image/png, image/jpeg"/>
        </p>
        <p>
            <label for="description_id">Description</label> :
            <textarea name="description" id="description_id" rows="4" cols="50" required>Veuillez entrer une description...</textarea>
        </p>
        <p>
            <label for="puissance_id">Puissance</label> :
            <input type="number" placeholder="1100" name="puissance" id="puissance_id" required/>
        </p>
        <p>
            <label for="consommation_id">Consommation électrique</label> :
            <input type="number" placeholder="1100" name="consommation" id="consommation_id" required/>
        </p>
        <p>
            <input type="submit" value="Envoyer" />
            <input type='hidden' name='action' value='created'>
            <input type='hidden' name='controller' value='souffleur'>
    </fieldset>
</form>
</div>