<?php
namespace Ecommerce\src\Controller;

use Ecommerce\src\Model\DataObject\Souffleur;
use Ecommerce\src\Model\Repository\SouffleurRepository;

class ControllerSouffleur extends AbstractController
{



    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function readAll() : void {
        $souffleurs = (new SouffleurRepository())->selectAll(); //appel au modèle pour gerer la BD
        //require __DIR__ . '/../view/Souffleur/list.php';  //"redirige" vers la vue
        ControllerSouffleur::afficheVue('../view/view.php',["souffleurs"=>$souffleurs,'pagetitle'=>"Liste des Souffleurs", "cheminVueBody"=>"Produit/shop.php"]);
    }

    public static function  read() : void {
            $idSouffleur =$_GET['idSouffleur'];
            $Souffleur = (new SouffleurRepository())->select($idSouffleur);
            if(is_null($Souffleur) ){
                ControllerSouffleur::afficheVue('../view/view.php',['pagetitle'=>"Détails du Souffleur", "cheminVueBody"=>"Produit/error.php"]);
            }else{
                $parametre =array($Souffleur);
                ControllerSouffleur::afficheVue('../view/view.php',['Souffleur'=>$Souffleur,'pagetitle'=>"Détails de la Souffleur", "cheminVueBody"=>"Produit/detail.php"]);
            }
    }

    public static function buy() : void {
        ControllerSouffleur::afficheVue('../view/view.php',['pagetitle'=>"Acheter une Souffleur", "cheminVueBody"=>"Produit/buy.php"]);
    }

    public static function  create() : void {
        ControllerSouffleur::afficheVue('../view/view.php',['pagetitle'=>"Créer une Souffleur", "cheminVueBody"=>"Produit/create.php"]);
    }

    public static function created() : void {
        $Souffleur = new Souffleur(0,$_GET['nomProduit'],$_GET['descriptionProduit'],$_GET['prixProduit'],$_GET['imageProduit'],$_GET['puissance'],$_GET['consommation']);
        $creer = (new SouffleurRepository)->insertWithoutId($Souffleur);
        if ( $creer) ControllerSouffleur::afficheVue('../view/view.php',['pagetitle'=>"Souffleur créée", "cheminVueBody"=>"Produit/created.php"]);
        else{
            ControllerSouffleur::afficheVue('../view/view.php',['pagetitle'=>"Souffleur non créée", "cheminVueBody"=>"Produit/error.php"]);
        }
    }

    public static function error( string $errorMessage ="") {
        
        ControllerSouffleur::afficheVue('../view/Souffleur/error.php',['message'=>$errorMessage]);
    }
    public static function delete() : void {
        ControllerSouffleur::afficheVue('../view/view.php',['pagetitle'=>"Créer une Souffleur", "cheminVueBody"=>"Produit/delete.php"]);
    }

    public static function deleted() : void {
        $idSouffleur=$_GET['idSouffleur'];
        $supprimer= (new SouffleurRepository())->delete($idSouffleur);
        if($supprimer){
            ControllerSouffleur::afficheVue('../view/view.php',['pagetitle'=>"Souffleur supprimée", "cheminVueBody"=>"Produit/deleted.php", 'idSouffleur'=>$idSouffleur]);
        }else{
            ControllerSouffleur::afficheVue('../view/view.php',['pagetitle'=>"Souffleur non supprimée", "cheminVueBody"=>"Produit/error.php", "message"=>'Souffleur non supprimée']);
        }
    }

    public static function update() : void {
        $idSouffleur=$_GET['idproduit'];
        $Souffleur=(new SouffleurRepository())->selectSouffleur($idSouffleur);
        if($Souffleur != null){

            ControllerSouffleur::afficheVue('../view/view.php',['pagetitle'=>"Modifier un Souffleur", "cheminVueBody"=>"Produit/Souffleur/update.php", 'souffleur'=>$Souffleur]);
        }
        else{
            ControllerSouffleur::afficheVue('../view/view.php',['pagetitle'=>"Souffleur non supprimé", "cheminVueBody"=>"Produit/error.php"]);
        }

    }

    public static function updated() : void {
        $Souffleur = new Souffleur($_GET['idProduit'],$_GET['nomProduit'],$_GET['descriptionProduit'],$_GET['prixProduit'],$_GET['imageProduit'],$_GET['puissance'],$_GET['consommation']);
        
        $update=(new SouffleurRepository)->updateSouffleur($Souffleur);
        if($update){ControllerSouffleur::afficheVue('../view/view.php',['pagetitle'=>"Souffleur modifiée", "cheminVueBody"=>"Produit/updated.php",'id'=>$_GET['idProduit']]);}
        else{
            ControllerSouffleur::afficheVue('../view/view.php',['pagetitle'=>"Souffleur non modifiée", "cheminVueBody"=>"Produit/error.php"]);
        }
    }

    public static function default(): void
    {
        // TODO: Implement default() method.
    }
}
?>











