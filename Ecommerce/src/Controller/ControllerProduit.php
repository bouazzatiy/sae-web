<?php
namespace Ecommerce\src\Controller;

use Ecommerce\src\Model\DataObject\Panier;
use Ecommerce\src\Model\DataObject\produit;
use Ecommerce\src\Model\Repository\FeuilleRepository;
use Ecommerce\src\Model\Repository\ProduitRepository;
use Ecommerce\src\Model\Repository\OffreRepository;
use Ecommerce\src\Model\Repository\SouffleurRepository;
use Ecommerce\src\Lib\MessageFlash;

class Controllerproduit extends AbstractController {

    public static function default() : void {
        self::shop();
    }

    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function shop() : void {
        $produits =(new ProduitRepository())->selectAll();

        parent::afficheVue('../view/view.php',["produits"=>$produits,'pagetitle'=>"Liste des produits", "cheminVueBody"=>"Produit/shop.php"]);
    }

    public static function readAll() : void {
        $produits =(new FeuilleRepository())->selectAllFeuille();
        $produits = array_merge($produits, (new OffreRepository())->selectAllOffre());
        $produits = array_merge($produits, (new SouffleurRepository())->selectAllSouffleur());
        parent::afficheVue('../view/view.php',["produits"=>$produits,'pagetitle'=>"Liste des produits", "cheminVueBody"=>"Produit/list.php"]);
    }

    public static function read() : void {
            $idproduit =$_GET['idproduit'];
            $produit = (new ProduitRepository())->select($idproduit);
            if(is_null($produit) ){
                parent::afficheVue('../view/view.php',['pagetitle'=>"Détails du produit", "cheminVueBody"=>"Produit/error.php"]);
            }else{
                $parametre =array($produit);
                parent::afficheVue('../view/view.php',['produit'=>$produit,'pagetitle'=>"Détails de la produit", "cheminVueBody"=>"Produit/detail.php"]);
            }
    }

    public static function buy() : void {
        parent::afficheVue('../view/view.php',['pagetitle'=>"Acheter une produit", "cheminVueBody"=>"Produit/buy.php"]);
    }

    public static function  create() : void {
        //Controllerproduit::afficheVue('produit/create.php');
        $listeProduit = (new ProduitRepository())->selectAll();
        parent::afficheVue('../view/view.php',['listeProduit'=>$listeProduit,'pagetitle'=>"Créer une produit", "cheminVueBody"=>"Produit/create.php"]);
    }

    public static function created() : void {
        $produit = new produit($_GET['idProduit'],$_GET['nomProduit'],$_GET['prixProduit'],$_GET['prixProduit'],$_GET['descriptionProduit'],$_GET['imageProduit']);
        $creer = (new ProduitRepository())->insert($produit);        
        if ( $creer) parent::afficheVue('../view/view.php',['pagetitle'=>"produit créée", "cheminVueBody"=>"Produit/created.php"]);
        else{
            parent::afficheVue('../view/view.php',['pagetitle'=>"Produit non créé", "cheminVueBody"=>"Produit/error.php"]);
        }
    }

    public static function error( string $errorMessage = "Une erreur innatendue s'est produite") {

        parent::afficheVue('../view/Produit/error.php',['message'=>$errorMessage]);
    }

    public static function error2( string $errorMessage = "Une erreur innatendue s'est produite") {

        parent::afficheVue('../view/view.php',['message'=>$errorMessage, 'pagetitle' => "Erreur", "cheminVueBody"=>"Produit/error.php"]);
    }

    public static function delete() : void {
        parent::afficheVue('../view/view.php',['pagetitle'=>"Créer une produit", "cheminVueBody"=>"Produit/delete.php"]);
    }

    public static function deleted() : void {
        $idproduit=$_GET['idproduit'];
        $supprimer= (new ProduitRepository())->delete($idproduit);
        if($supprimer){
            parent::afficheVue('../view/view.php',['pagetitle'=>"produit supprimée", "cheminVueBody"=>"Produit/deleted.php", 'idproduit'=>$idproduit]);
        }else{
            parent::afficheVue('../view/view.php',['pagetitle'=>"produit non supprimée", "cheminVueBody"=>"Produit/error.php", "message"=>'produit non supprimée']);
        }
    }

    public static function update() : void {
        $typeproduit=$_GET['typeproduit'];
        
        if($typeproduit != null){
            $controllerClassName='Ecommerce\src\Controller\Controller'.ucfirst($typeproduit);
            $controllerClassName::update();
            //parent::afficheVue('../view/view.php',['pagetitle'=>"Modifier un produit", "cheminVueBody"=>"Produit/{$typeproduit}/update.php", 'idproduit'=>$idproduit]);
        }
        else{
            parent::afficheVue('../view/view.php',['pagetitle'=>"produit non supprimé", "cheminVueBody"=>"Produit/error.php"]);
        }

    }

    public static function updated() : void {
        (new produitRepository())->update(new produit($_GET['idProduit'],$_GET['nomProduit'],$_GET['prixProduit'],$_GET['prixProduit'],$_GET['descriptionProduit'],$_GET['imageProduit']));
        parent::afficheVue('../view/view.php',['pagetitle'=>"produit modifiée", "cheminVueBody"=>"Produit/updated.php",'immatriculation'=>$_GET['immat']]);
    }

    public static function add() : void {
        $idProduit=$_GET['idProduit'];
        Panier::ajouter($idProduit);

        MessageFlash::ajouter("success", "Le produit a bien été ajouté au panier.");
        self::buy();
    }
}
?>











