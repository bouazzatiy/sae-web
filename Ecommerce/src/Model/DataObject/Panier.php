<?php

namespace Ecommerce\src\Model\DataObject;

use Exception;
use Ecommerce\src\Model\Repository\ProduitRepository;
use Ecommerce\src\Model\HTTP\Session;
class Panier {

    
    private static string $cle = "panier";

    public static function ajouter(int $idProduit, ?int $nbProduit = 1) : void
    {
        $s = Session::getInstance();

        $arrayPanier = [];
        if ($s->contient(self::$cle))
        {
            $arrayPanier = $s->lire(self::$cle);
        }

        if(isset($arrayPanier[$idProduit]))
        {
            $arrayPanier[$idProduit] += $nbProduit;
        }
        else
        {
            $arrayPanier[$idProduit] = $nbProduit;
        }

        $s->enregistrer(self::$cle, $arrayPanier);
    }

    public static function lire() : array
    {
        $arrayPanier = Session::getInstance()->lire(static::$cle);

        $panier = [];
        foreach(array_keys($arrayPanier) as $key)
        {
            $panier[] = [(new ProduitRepository())->select($key), $arrayPanier[$key]];
        }
       return $panier;
    }

    public static function viderPanier() {
        Session::getInstance()->supprimer(static::$cle);
    }


    /*private static ?Panier $instance = null;

    private function __construct()
    {
        if (session_start() == false) {
            throw new Exception("La session n'a pas réussi à démarrer.");
        }else{
            if (!isset($_SESSION['panier'])) {
                $_SESSION['panier'] = [];
            }
        }
    }

    public static function getInstance(): Panier
    {        
        if (is_null(static::$instance))
            static::$instance = new Panier();
        return static::$instance;
    }

    public function ajouterPanier($idProduit) {
        if (isset($_SESSION['panier'][$idProduit])) {
            $_SESSION['panier'][$idProduit] ++;
        } else {
            $_SESSION['panier'][$idProduit] = 1;
        }
    }

    public function getPanier(){
        $panier = [];
        foreach (array_keys($_SESSION['panier']) as $key ) {
            $panier[] = [(new ProduitRepository())->select($key),$_SESSION['panier'][$key]];
        }
        return $panier;
    }

    public function getIdProduits(){
        return array_keys($_SESSION['panier']);
    }

    public function supprimerPanier($idProduit) {
        unset($_SESSION['panier'][$idProduit]);
    }

    public function viderPanier() {
        unset($_SESSION['panier']);
    }

    public function reduirePanier($idProduit) {
        if (isset($_SESSION['panier'][$idProduit])) {
            $_SESSION['panier'][$idProduit] --;
        }
    }*/
}