<?php

namespace Ecommerce\src\Model\DataObject;

class Souffleur extends Article
{
    private float $puissance;
    private float $consommationElectrique;

    public function __construct(int $id, string $nom, string $description, int $prix, string $image, float $puissance, float $consommationElectrique)
    {
        parent::__construct($id, $nom, $description, $prix, $image);
        $this->puissance = $puissance;
        $this->consommationElectrique = $consommationElectrique;
    }

    public function formatTableau(): array
    {
        $souffleurFormatTableau = parent::formatTableau();
        $souffleurFormatTableau['puissance'] = $this->puissance;
        $souffleurFormatTableau['consommationElectrique'] = $this->consommationElectrique;
        return $souffleurFormatTableau;
    }

    public function formatTableauWithoutId(): array
    {
        $souffleurFormatTableau = parent::formatTableauWithoutId();
        $souffleurFormatTableau['puissance'] = $this->puissance;
        $souffleurFormatTableau['consommationElectrique'] = $this->consommationElectrique;
        return $souffleurFormatTableau;
    }

    public function getPuissance(): float
    {
        return $this->puissance;
    }

    public function setPuissance(float $puissance)
    {
        $this->puissance = $puissance;
    }

    public function getConsommationElectrique(): float
    {
        return $this->consommationElectrique;
    }

    public function setConsommationElectrique(float $consommationElectrique)
    {
        $this->consommationElectrique = $consommationElectrique;
    }
    public function getTypeproduit(): string
    {
        return 'souffleur';
    }
    
}