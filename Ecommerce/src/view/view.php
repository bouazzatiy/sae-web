<?php

use Ecommerce\src\Lib\ConnexionUtilisateur;

if (session_status() === PHP_SESSION_NONE) {
    session_start();
}else{
    if (!isset($_SESSION['panier'])) {
        $_SESSION['panier'] = [];
    }
}
?><!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo $pagetitle; ?></title>
        <link href="css/style2.css" rel="stylesheet" type="text/css">
        <link rel="icon" type="image/x-icon" href="image/icone.ico">

    </head>
    <body>
        <header>

            <div id="header-content">

                <h1 id="title">LEAF TRADING REVOLUTION</h1>

                <nav id="navbar">
                    <a href=".">Page d'accueil</a>
                    <a href=".?controller=Produit">Shop</a>
                    <a href=".?controller=Panier">Mon panier</a>
                    <?php
                    if (ConnexionUtilisateur::estConnecte()) {
                        echo '<a href="?controller=Client&action=deconnexion">Mon compte</a>';
                    } 
                    else {
                        echo '<a href="?controller=Client&action=login">Se connecter</a>';
                    }
                    ?>
                </nav>

            </div>

        </header>
        <main>

            <?php

            foreach ($types as $type){

                if (isset($messagesFlash[$type]))
                {
                    foreach ($messagesFlash[$type] as $message)
                    echo "<div class='$type'>$message</div>";
                }
            }
            ?>

            <?php
            require __DIR__ . "/{$cheminVueBody}";
            ?>

        </main>
        <footer>

            <div>
                <h1>Soufflez !</h1>
                <p>
                    Une boutique de feuilles soufflées avec passion.
                </p>
            </div>

            <div>
                <h2>À propos</h2>
                <a class="lienSimple" href="?action=contexte">Le contexte</a>
                <a class="lienSimple" href="?action=equipe">Notre équipe</a>
            </div>

            <div>
                <h2>Naviguez</h2>
                <a class="lienSimple" href=".">Page d'accueil</a>
                <a class="lienSimple" href=".?controller=Produit">Shop</a>
            </div>

        </footer>
    </body>
</html>

