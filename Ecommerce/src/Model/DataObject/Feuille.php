<?php

namespace Ecommerce\src\Model\DataObject;

class Feuille extends Article
{
    private int $masse;

    public function __construct(int $id, string $nom, string $description, int $prix, string $image, int $masse)
    {
        parent::__construct($id, $nom, $description, $prix, $image);
        $this->masse = $masse;
    }

    public function formatTableau(): array
    {
        $feuilleFormatTableau = parent::formatTableau();
        $feuilleFormatTableau['masse'] = $this->masse;
        return $feuilleFormatTableau;
    }

    public function formatTableauWithoutId(): array
    {
        $feuilleFormatTableau = parent::formatTableauWithoutId();
        $feuilleFormatTableau['masse'] = $this->masse;
        return $feuilleFormatTableau;
    }

    public function getMasse(): int
    {
        return $this->masse;
    }

    public function setMasse(int $masse)
    {
        $this->masse = $masse;
    }
    public function getTypeproduit(): string
    {
        return 'feuille';
    }

}