<?php

namespace Ecommerce\src\Lib;

use Ecommerce\src\Model\HTTP\Session;

class MessageFlash
{

    // Les messages sont enregistrés en session associée à la clé suivante
    private static string $cleFlash = "_messagesFlash";

    public static array $types = ["success", "info", "warning", "danger"];

    // $type parmi "success", "info", "warning" ou "danger"
    public static function ajouter(string $type, string $message): void
    {
        $cle = $type . static::$cleFlash;
        $messagesDejaPresents = [];

        if (Session::getInstance()->contient($cle))
        {
            $messagesDejaPresents = Session::getInstance()->lire($cle);
        }

        $messagesDejaPresents[] = $message;

        Session::getInstance()->enregistrer($cle, $messagesDejaPresents);
    }

    public static function contientMessage(string $type): bool
    {
        return Session::getInstance()->contient($type . static::$cleFlash);
    }

    // Attention : la lecture doit détruire le message
    public static function lireMessages(string $type): array
    {
        $cle = $type . static::$cleFlash;

        if (Session::getInstance()->contient($cle))
        {
            $messages = Session::getInstance()->lire($cle);
            Session::getInstance()->supprimer($cle);
        }
        else
        {
            $messages = [];
        }

        return $messages;
    }

    public static function lireTousMessages() : array
    {
        $messages = [];
        foreach (static::$types as $type)
        {
            $messages[$type] = self::lireMessages($type);
        }

        return $messages;
    }
}