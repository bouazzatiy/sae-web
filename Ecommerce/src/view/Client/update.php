<?php
    /** @var Client $client*/
?>
<form method="get" >
    <fieldset>
        <legend>Modifier le client:</legend>
        <p>
            <label for="id_id">Identifiant</label> :
            <input type="text" value=<?= htmlspecialchars($client->getId()) ?> name="id" id="id_id" readonly required/>
        </p>
        <p>
            <label for="nom_id">Nom</label> :
            <input type="text" value=<?= htmlspecialchars($client->getNom()) ?> name="nom" id="nom_id"/>
        </p>
        <p>
            <label for="prenom_id">Prenom</label> :
            <input type="text" value=<?= htmlspecialchars($client->getPrenom()) ?> name="prenom" id="prenom_id" required/>
        </p>
        <p>
            <label for="email_id">Email</label> :
            <input type="email" value=<?= htmlspecialchars($client->getEmail()) ?>  name="email" id="email_id" size="30" required/>
        </p>
        <p>
            <input type="submit" value="Envoyer" />
            <input type='hidden' name='action' value='updated'>
            <input type='hidden' name='controller' value='client'>
        </p>
    </fieldset>
</form>
