<?php
namespace Ecommerce\src\Controller;

use Ecommerce\src\Lib\MessageFlash;
use Ecommerce\src\Lib\MotDePasse;
use Ecommerce\src\Lib\ConnexionUtilisateur;
use Ecommerce\src\Model\DataObject\Client;
use Ecommerce\src\Model\HTTP\Session;
use Ecommerce\src\Model\Repository\ClientRepository;

class ControllerClient extends AbstractController
{

    public static function default() : void {
        self::readAll();
    }

    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function readAll() : void {
        $Clients = (new ClientRepository())->selectAll(); //appel au modèle pour gerer la BD
        //require __DIR__ . '/../view/Client/list.php';  //"redirige" vers la vue
        ControllerClient::afficheVue('../view/view.php',["Clients"=>$Clients,'pagetitle'=>"Liste des Clients", "cheminVueBody"=>"Client/list.php"]);
    }

    public static function read() : void {
            $id =$_GET['id'];
            $Client = (new ClientRepository())->select($id);
            if(is_null($Client) ){
                ControllerClient::afficheVue('../view/view.php',['pagetitle'=>"Détails du Client", "cheminVueBody"=>"Client/error.php"]);
            }else{
                $parametre =array($Client);
                ControllerClient::afficheVue('../view/view.php',['client'=>$Client,'pagetitle'=>"Détails de la Client", "cheminVueBody"=>"Client/detail.php"]);
            }
    }

    public static function  create() : void {
        //ControllerClient::afficheVue('Client/create.php');
        ControllerClient::afficheVue('../view/view.php',['pagetitle'=>"Créer une Client", "cheminVueBody"=>"Client/create.php"]);
    }

    public static function created() : void {
        $Client = new Client(2,$_GET['nom'],$_GET['prenom'],$_GET['email'],$_GET['password'],$_GET['admin']);
        $creer = (new ClientRepository())->insertWithoutId($Client);
        if ( $creer) ControllerClient::afficheVue('../view/view.php',['pagetitle'=>"Client créée", "cheminVueBody"=>"Client/created.php"]);
        else{
            ControllerClient::afficheVue('../view/view.php',['pagetitle'=>"Client non créée", "cheminVueBody"=>"Client/error.php"]);
        }
    }

    public static function error( string $errorMessage ="") {
        ControllerClient::afficheVue('../view/view.php',['pagetitle'=>"Erreur", "cheminVueBody"=>"../view/Client/error.php",'message'=>$errorMessage]);
    }

    public static function delete() : void {
        ControllerClient::afficheVue('../view/view.php',['pagetitle'=>"Créer une Client", "cheminVueBody"=>"Client/delete.php"]);
    }

    public static function deleted() : void {
        $id=$_GET['id'];
        $supprimer= (new ClientRepository())->delete($id);
        if($supprimer){
            ControllerClient::afficheVue('../view/view.php',['pagetitle'=>"Client supprimée", "cheminVueBody"=>"Client/deleted.php", 'id'=>$id]);
        }else{
            ControllerClient::afficheVue('../view/view.php',['pagetitle'=>"Client non supprimée", "cheminVueBody"=>"Client/error.php", "message"=>'Client non supprimée']);
        }
    }

    public static function update() : void {
        $id=$_GET['id'];
        $client=(new ClientRepository())->select($id);

        if($client != null){

            ControllerClient::afficheVue('../view/view.php',['pagetitle'=>"Modifier un Client", "cheminVueBody"=>"Client/update.php", 'client'=>$client]);
        }
        else{
            ControllerClient::afficheVue('../view/view.php',['pagetitle'=>"Client non supprimé", "cheminVueBody"=>"Client/error.php"]);
        }

    }

    public static function updated() : void {
        (new ClientRepository())->updateWithoutPassword(new Client($_GET['id'],$_GET['nom'],$_GET['prenom'],$_GET['email'], "", $_GET['admin']));
        ControllerClient::afficheVue('../view/view.php',['pagetitle'=>"Client modifiée", "cheminVueBody"=>"Client/updated.php",'id'=>$_GET['id']]);

    }

    // méthodes pour se connecter et s'enregistrer *****************************************************

    public static function login() : void {
        ControllerClient::afficheVue('../view/view.php',['pagetitle'=>"Se connecter", "cheminVueBody"=>"Client/formulaireConnexion.php"]);
    }

    public static function connecter() : void {
        $client = (new ClientRepository())->selectByEmail($_GET['email']);
        if ($client != null && MotDePasse::verifier($_GET['password'], $client->getMotDePasse())) {
            ControllerClient::afficheVue('../view/view.php', ['pagetitle' => 'Connecté', 'cheminVueBody' => 'Client/connecter.php']);
            ConnexionUtilisateur::connecter($client->getId());
            Session::getInstance()->enregistrer('_admin', $client->getAdmin());
        } else {
            ControllerClient::afficheVue('../view/view.php', ['cheminVueBody' => 'Client/error.php', 'message' => 'Identifiants incorrects']);
        }
    }

    public static function register() : void {
        ControllerClient::afficheVue('../view/view.php', ['pagetitle' => 'Créer un compte', 'cheminVueBody' => 'Client/register.php']);
    }

    public static function registered() : void {
        if ($_GET['password'] == $_GET['password2']) {
            $client = new client(0, $_GET['nom'], $_GET['prenom'], $_GET['email'], MotDePasse::hacher($_GET['password']), false);
            $registered = (new ClientRepository())->insertWithoutId($client);
            if ($registered) {

                MessageFlash::ajouter("success", "Compte créé avec succès");


                ControllerClient::afficheVue('../view/view.php', ['pagetitle' => 'Compte créé', 'cheminVueBody' => 'Client/registered.php']);
            } else {
                MessageFlash::ajouter("danger", "Identifiants incorrects");
                self::register();
            }
        } else {
            self::register();
        }
    }

    public static function deconnexion() : void {
        ConnexionUtilisateur::deconnecter();
        Session::getInstance()->supprimer('_admin');
    }
}