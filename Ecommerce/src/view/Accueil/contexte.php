<div>
    <h2>Un projet scolaire</h2>

    <div class="section">
        
        <img class="image_contexte" src="image/logoUM.jpg">

        <div class="contenuSection">
            <p>
                Dans le cadre de la <a class="lienSimple" href="https://romainlebreton.github.io/R3.01-DeveloppementWeb/projet.html">ressource 3.01</a> de la deuxième année du BUT informatique,
                nous avons été chargé de réaliser un site web d'e-commerce mettant en oeuvre les compétences aquises au cours du troisième semestre.
            </p>
        </div>

    </div>
</div>

<div>

    <h2>Le choix du marché</h2>

    <div class="section">

        <div class="contenuSection">
            <p>
                Après mûre réflexion et moulte altercations, l'équipe <a href="?action=equipe" class="lienSimple"><i>Propassif Capybara</i></a> a décidé de se pencher sur le commerce de feuilles d'arbres
                mystiques. Grandement inspirée (voire totalement larciné) de la mythologie de l'univers vidéoludique de <a class="lienSimple" href="#leafBlowerRevolution">Leaf Blower Revolution</a>, ce site permettra donc
                l'achat de ces feuilles aux propriétés et origines mystérieuses...
            </p>
        </div>

        <img class="image_contexte" src="image/autumnLeavesMeme.webp" title="Spoiler : elles valent bien plus.">

    </div>

</div>

<div id="leafBlowerRevolution">

    <h2>Leaf Blower Revolution</h2>

    <div class="section">

        <img class="image_contexte" src="image/logoLBR.png"">

        <div class="contenuSection">
            <p>
                Leaf Blower Revolution est un jeu vidéo dans lequel on incarne le voisin de Bob cherchant à se débarasser des feuilles qui s'accumulent dans son jardin.
                Au fil de son labeur, son intérêt pour ces feuilles va grandir et sa curiosité va prendre le dessus. Notre protagoniste va développer une fixation pour celles-ci, au point
                de mettre en place divers instruments de recherches occultes. Ce jeu va donc suivre l'épopée de cette personne banale qui, cherchant à découvrir la vérité sur cet univers feuillu,
                va se plonger dans des environnements profondéments étrangers qui dépassent sa compréhension.
            </p>

            <p>
                Ce jeu est développé et édité par <a class="lienSimple" href="https://humblenorth.de/">Humble North</a> et est disponible sur <a href="https://s.team/a/1468260" class="lienSimple">Steam</a>
                , sur l'<a href="https://apps.apple.com/app/id6443698340" class="lienSimple">App Store</a> ainsi que sur <a class="lienSimple" href="https://play.google.com/store/apps/details?id=com.ezplugins.leafblowerrevolution">Google Play</a>.
            </p>
        </div>

    </div>

</div>

<div>

    <h2>Des images générées par IA</h2>

    <div class="section">

        <div class="contenuSection">

            <p>
                Afin de représenter certains de nos produit, nous avons décidé de générer nos images grâce à une intelligence artificielle. Nous avons ainsi pu représenter nos feuilles les plus
                fantaisistes tout en gardant un style réaliste.
            </p>

            <p>
                Notre choix s'est porté sur <a class="lienSimple" href="https://openai.com/dall-e-2/">Dall-E 2</a> de <a class="lienSimple" href="https://openai.com/">OpenAI</a>,
                une IA gratuite permettant la génération d'images libre de droits.
            </p>

            <p>
                Les images ayant été générées de cette manière sont toutes marquées avec le filigrane de Dall-E 2.
            </p>

        </div>

        <img class="image_contexte" src="image/logoDallE.png">

    </div>

</div>
