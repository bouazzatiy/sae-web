<?php

namespace Ecommerce\src\Model\DataObject;

class Article extends Produit {

    public function __construct(int $id, string $nom, string $description, int $prix, string $image)
    {
        parent::__construct($id, $nom, $description, $prix, $image);
    }

    public function formatTableau(): array
    {
        $articleFormatTableau = parent::formatTableau();
        return $articleFormatTableau;
    }
}