<div>
    <h2>Inscription</h2>

    <form method="get">


        <div class="centreurBouton">

            <div class="contenuForm">
                <p>
                    <label for="nom_id">Nom</label>
                    <input type="text" placeholder="Humble" name="nom" id="nom_id" required/>
                </p>
                <p>
                    <label for="prenom_id">Prénom</label>
                    <input type="text" placeholder="Bob" name="prenom" id="prenom_id" required/>
                </p>
                <p>
                    <label for="email_id">Adresse mail</label>
                    <input type="email" placeholder="bob@feuille.com" name="email" id="email_id" required/>
                </p>
                <p>
                    <label for="password_id">Mot de passe</label>
                    <input type="password" placeholder="*******" name="password" id="password_id" required/>
                </p>
                <p>
                    <label for="password2_id">Confirmer mot de passe</label>
                    <input type="password" placeholder="*******" name="password2" id="password2_id" required/>
                </p>
            </div>

        </div>



        <div class="centreurBouton">
            <input type="submit" value="Envoyer" class="boutonStyleLBR"/>
            <input type='hidden' name='action' value='registered'>
            <input type='hidden' name='controller' value='client'>
        </div>

    </form>

    <p>
        Déjà membre ? <a class="lienSimple" href="?controller=Client&action=login">S'identifier par ici !</a>
    </p>

</div>