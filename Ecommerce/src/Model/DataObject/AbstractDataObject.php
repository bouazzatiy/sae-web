<?php

namespace Ecommerce\src\Model\DataObject;

abstract class AbstractDataObject
{
    public abstract function formatTableau(): array; 
    public abstract function formatTableauWithoutId(): array; 

    public abstract function getId(): string;
    public abstract function getNom(): string; 

    public abstract function setId(string $id);
    public abstract function setNom(string $nom);
   

}