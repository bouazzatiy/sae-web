<?php

namespace Ecommerce\src\Model\Repository;

use Ecommerce\src\Model\DataObject\Article;

class ArticleRepository extends AbstractRepository
{
    protected function construire(array $articleFormatTableau): Article
    {
        return new Article(
            $articleFormatTableau['idProduit'],
            $articleFormatTableau['nomProduit'],
            $articleFormatTableau['prixProduit'],
            $articleFormatTableau['descriptionProduit'],
            $articleFormatTableau['imageProduit']
        );
    }

    protected function getNomTable(): string
    {
        return 'p_vueArticles';
    }

    protected function getNomClePrimaire(): string
    {
        return 'idProduit';
    }

    protected function getNomsColonnes(): array
    {
        return array(
            'idProduit',
            'nomProduit',
            'descriptionProduit',
            'prixProduit',
            'imageProduit'
        );
    }
}