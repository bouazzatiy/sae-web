<?php

namespace Ecommerce\src\Model\Repository;

use Ecommerce\src\Model\DataObject\Souffleur;
use PDOException;

class SouffleurRepository extends AbstractRepository
{
    protected function construire(array $souffleurFormatTableau): Souffleur
    {
        return new Souffleur(
            $souffleurFormatTableau['idProduit'],
            $souffleurFormatTableau['nomProduit'],
            $souffleurFormatTableau['descriptionProduit'],
            $souffleurFormatTableau['prixProduit'],
            $souffleurFormatTableau['imageProduit'],
            $souffleurFormatTableau['puissanceSouffleur'],
            $souffleurFormatTableau['consommationElectrique']
        );
    }

    protected function getNomTable(): string
    {
        return 'p_vueSouffleurs';
    }

    protected function getNomClePrimaire(): string
    {
        return 'idProduit';
    }

    protected function getNomsColonnes(): array
    {
        return array(
            'idProduit',
            'nomProduit',
            'descriptionProduit',
            'prixProduit',
            'imageProduit',
            'puissanceSouffleur',
            'consommationElectrique'
        );
    }

    public function selectAllSouffleur(): array
    {
        $sql = 'SELECT idProduit, nomProduit, descriptionProduit, prixProduit, imageProduit, puissanceSouffleur, consommationElectrique FROM p_souffleurs JOIN p_produits ON idsouffleur = idProduit';
        $pdoStatement = DatabaseConnection::getPdo()->prepare($sql);
        
        $pdoStatement->execute();

        $objets = array();
        foreach ($pdoStatement as $objetFormatTableau)
        {
            $objets[] = $this->construire($objetFormatTableau);
        }
        return $objets;
    }
    public function insertWithoutId(Souffleur $objet) : bool
    {
        $sql = "INSERT INTO p_produits (nomProduit, descriptionProduit, prixProduit, imageProduit) VALUES (:nomProduit, :descriptionProduit, :prixProduit, :imageProduit);
                INSERT INTO p_articles (idArticle) VALUES (LAST_INSERT_ID());
                INSERT INTO p_souffleurs (idSouffleur, puissanceSouffleur, consommationElectrique) VALUES (LAST_INSERT_ID(), :puissance, :consommation);";

        
        $pdoStatement = DatabaseConnection::getPdo()->prepare($sql);
        
        try{
            $pdoStatement->execute($objet->formatTableauWithoutId());
        }catch(PDOException $e){
            return false;
        }
        return true;
    }

    public function selectSouffleur(string $valeurClePrimaire): Souffleur
    {
        $sql = 'SELECT idProduit, nomProduit, descriptionProduit, prixProduit, imageProduit, puissanceSouffleur, consommationElectrique
         FROM p_souffleurs 
         JOIN p_produits ON idSouffleur = idProduit WHERE idSouffleur = :valeurClePrimaire';
        $pdoStatement = DatabaseConnection::getPdo()->prepare($sql);
        $values = array(
            "valeurClePrimaire" => $valeurClePrimaire
        );
        $pdoStatement->execute($values);

        $objet = $pdoStatement->fetch();
        if ($objet)
            return $this->construire($objet);
        else
            return null;
    }
    public function updateSouffleur(Souffleur $souffleur): bool
    {
        $sql = 
            'UPDATE p_souffleurs 
             SET puissanceSouffleur = :puissanceSouffleur, consommationElectrique = :consommationElectrique
             WHERE idSouffleur = :idProduit;
             UPDATE p_produits
             SET nomProduit = :nomProduit, descriptionProduit = :descriptionProduit, prixProduit = :prixProduit
             WHERE idProduit = :idProduit';
        $pdoStatement = DatabaseConnection::getPdo()->prepare($sql);
        try{
            $pdoStatement->execute($souffleur->formatTableau());;
        }catch(PDOException $e){
            return false;
        }
        return true;
    }
}