<?php

namespace Ecommerce\src\Model\DataObject;

class Offre extends Produit
{
    public function __construct(int $id, string $nom, string $description, int $prix, string $image)
    {
        parent::__construct($id, $nom, $description, $prix, $image);
    }

    public function formatTableau(): array
    {
        $feuilleFormatTableau = parent::formatTableau();
        return $feuilleFormatTableau;
    }
    public function getTypeproduit(): string
    {
        return 'offre';
    }
}