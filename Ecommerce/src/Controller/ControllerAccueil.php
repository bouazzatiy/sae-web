<?php

namespace Ecommerce\src\Controller;

class ControllerAccueil extends AbstractController
{
    public static function default() : void {
        self::accueil();
    }

    private static function accueil() :void
    {
        self::afficheVue('view.php', ['pagetitle' => "Leaf Trading Revolution", "cheminVueBody" => "Accueil/accueil.php"]);
    }

    public static function equipe() :void
    {
        self::afficheVue('view.php', ['pagetitle' => "Notre équipe", "cheminVueBody" => "Accueil/equipe.php"]);
    }

    public static function contexte() :void
    {
        self::afficheVue('view.php', ['pagetitle' => "Contexte", "cheminVueBody" => "Accueil/contexte.php"]);
    }
}