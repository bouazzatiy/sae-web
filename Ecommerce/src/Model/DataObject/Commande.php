<?php

namespace Ecommerce\src\Model\DataObject;

class Commande 
{
    private static ?Commande $instance = null;
    private int $idcommande;
    private int $idclient;
    private array $produits;
    private int $prixTotal;

    private function __construct(int $idcommande,int $idclient, array $produits, int $prix)
    {
        $this->idcommande = $idcommande;
        $this->idclient = $idclient;
        $this->produits = $produits;
        $this->prixTotal = $prix;
    }

    public static function getInstance(): Commande
    {        
        if (is_null(static::$instance)){
            
            static::$instance = new Commande(0, 0,[], 0);
        }  
        return static::$instance;
    }

    public function getIdCommande(): int
    {
        return $this->idcommande;
    }

    public function setIdCommande(int $idcommande)
    {
        $this->idcommande = $idcommande;
    }
    public function getIdClient(): int
    {
        return $this->idclient;
    }

    public function setIdClient(int $idclient)
    {
        $this->idclient = $idclient;
    }

    public function getProduits(): array
    {
        return $this->produits;
    }

    public function setProduits(array $produits)
    {
        $this->produits = $produits;
    }

    public function getPrixTotal(): int
    {
        return $this->prixTotal;
    }

    public function setPrixTotal(int $prix)
    {
        $this->prixTotal = $prix;
    }

    public function addProduit(array $idProduit)
    {
        $this->produits[] = $idProduit;
    }

}