<?php

use Ecommerce\src\Model\DataObject\Commande;

echo " <div class='panier'>
        <table>
            <thead>
                <tr>
                    <th >Produit</th>
                    <th >Prix</th>
                    <th></th>
                    <th >Quantité</th> 
                    <th></th>               
                </tr>
            </thead>
            <tbody>
                
            ";
$prixTotal = 0;
foreach ($paniers as $produit) {
    $produitHTML=htmlspecialchars($produit[0]->getNom());
    $prixHTML=htmlspecialchars($produit[0]->getPrix());
    $quantiteHTML= htmlspecialchars($produit[1]);
    $idProduit = $produit[0]->getId();

    $prixTotal += $prixHTML * $quantiteHTML;

    echo "  
                <tr>
                <td >$produitHTML</td>
                <td >{$prixHTML}€</td>
                <td><a class='boutonCarreStyleLBR' href='?controller=Panier&action=remove&idProduit=$idProduit'>−</a></td>
                <td >$quantiteHTML</td>
                <td><a class='boutonCarreStyleLBR' href='?controller=Panier&action=add&idProduit=$idProduit'>+</a></td>
                </tr>
                 ";
        }

$_SESSION['prixTotal']= $prixTotal;
echo "
        <tr>
                <td >Total</td>
                <td >{$prixTotal}€</td>
                <td ></td> 
            </tbody>
        </table>
        
        <div class='centreurBouton'><a href=\"?controller=Panier&action=validate\" class='boutonStyleLBR'>Valider le panier</a></div>

        
        </div>";

?>