<?php
namespace Ecommerce\src\Controller;

use Ecommerce\src\Model\DataObject\Panier;
use Ecommerce\src\Model\Repository\PanierRepository;
use Ecommerce\src\Lib\MessageFlash;

class ControllerPanier extends AbstractController
{

    public static function default() : void {
        self::readAll();
    }

    public static function readAll() {
        $paniers = Panier::lire();
        ControllerPanier::afficheVue('../view/view.php',["paniers"=>$paniers,'pagetitle'=>"Mon panier", "cheminVueBody"=>"Produit/panier.php"]);
    
    }
 
    public static function add() : void {
        $idProduit=$_GET['idProduit'];
        Panier::ajouter($idProduit);

        MessageFlash::ajouter("success", "Le produit a bien été ajouté au panier.");
        self::default();
    }

    public static function remove() :void{
        $idProduit=$_GET['idProduit'];
        Panier::ajouter($idProduit, -1);

        MessageFlash::ajouter("success", "Un produit a bien été retiré du panier.");
        self::default();
    }

    public static function validate() : void {
        
        $creer = (new PanierRepository())->insert();
        if ($creer) {
            Panier::viderPanier();
            ControllerAccueil::default();
        } else {
            MessageFlash::ajouter("danger", "Erreur lors de la validation de la commande");
            self::default();
        }
        
    }
}