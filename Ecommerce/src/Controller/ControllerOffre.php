<?php
namespace Ecommerce\src\Controller;

use Ecommerce\src\Model\DataObject\Offre;
use Ecommerce\src\Model\Repository\OffreRepository;

class ControllerOffre extends AbstractController
{

    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function readAll() : void {
        $offres = (new OffreRepository())->selectAll(); //appel au modèle pour gerer la BD
        //require __DIR__ . '/../view/Offre/list.php';  //"redirige" vers la vue
        ControllerOffre::afficheVue('../view/view.php',["offres"=>$offres,'pagetitle'=>"Liste des Offres", "cheminVueBody"=>"Produit/shop.php"]);
    }

    public static function  read() : void {
            $idOffre =$_GET['idOffre'];
            $Offre = (new OffreRepository())->select($idOffre);
            if(is_null($Offre) ){
                ControllerOffre::afficheVue('../view/view.php',['pagetitle'=>"Détails du Offre", "cheminVueBody"=>"Produit/error.php"]);
            }else{
                $parametre =array($Offre);
                ControllerOffre::afficheVue('../view/view.php',['Offre'=>$Offre,'pagetitle'=>"Détails de la Offre", "cheminVueBody"=>"Produit/detail.php"]);
            }
    }

    public static function buy() : void {
        ControllerOffre::afficheVue('../view/view.php',['pagetitle'=>"Acheter une Offre", "cheminVueBody"=>"Produit/buy.php"]);
    }

    public static function  create() : void {
        ControllerOffre::afficheVue('../view/view.php',['pagetitle'=>"Créer une Offre", "cheminVueBody"=>"Produit/create.php"]);
    }

    public static function created() : void {
        $Offre = new Offre(0,$_GET['nomProduit'],$_GET['descriptionProduit'],$_GET['prixProduit'],$_GET['imageProduit'],$_GET['puissance'],$_GET['consommation']);
        $creer = (new OffreRepository)->insertWithoutId($Offre);
        if ( $creer) ControllerOffre::afficheVue('../view/view.php',['pagetitle'=>"Offre créée", "cheminVueBody"=>"Produit/created.php"]);
        else{
            ControllerOffre::afficheVue('../view/view.php',['pagetitle'=>"Offre non créée", "cheminVueBody"=>"Produit/error.php"]);
        }
    }

    public static function error( string $errorMessage ="") {
        
        ControllerOffre::afficheVue('../view/Offre/error.php',['message'=>$errorMessage]);
    }
    public static function delete() : void {
        ControllerOffre::afficheVue('../view/view.php',['pagetitle'=>"Créer une Offre", "cheminVueBody"=>"Produit/delete.php"]);
    }

    public static function deleted() : void {
        $idOffre=$_GET['idOffre'];
        $supprimer= (new OffreRepository())->delete($idOffre);
        if($supprimer){
            ControllerOffre::afficheVue('../view/view.php',['pagetitle'=>"Offre supprimée", "cheminVueBody"=>"Produit/deleted.php", 'idOffre'=>$idOffre]);
        }else{
            ControllerOffre::afficheVue('../view/view.php',['pagetitle'=>"Offre non supprimée", "cheminVueBody"=>"Produit/error.php", "message"=>'Offre non supprimée']);
        }
    }
    public static function update() : void {
        $idOffre=$_GET['idproduit'];
        $Offre=(new OffreRepository())->selectOffre($idOffre);
        if($Offre != null){

            ControllerOffre::afficheVue('../view/view.php',['pagetitle'=>"Modifier un Offre", "cheminVueBody"=>"Produit/Offre/update.php", 'offre'=>$Offre]);
        }
        else{
            ControllerOffre::afficheVue('../view/view.php',['pagetitle'=>"Offre non supprimé", "cheminVueBody"=>"Produit/error.php"]);
        }

    }

    public static function updated() : void {
        $Offre = new Offre($_GET['idProduit'],$_GET['nomProduit'],$_GET['descriptionProduit'],$_GET['prixProduit'],$_GET['imageProduit'],$_GET['puissance'],$_GET['consommation']);
        
        $update=(new OffreRepository)->updateOffre($Offre);
        if($update){ControllerOffre::afficheVue('../view/view.php',['pagetitle'=>"Offre modifiée", "cheminVueBody"=>"Produit/updated.php",'id'=>$_GET['idProduit']]);}
        else{
            ControllerOffre::afficheVue('../view/view.php',['pagetitle'=>"Offre non modifiée", "cheminVueBody"=>"Produit/error.php"]);
        }
    }

    public static function default(): void
    {
        // TODO: Implement default() method.
    }
}
?>











