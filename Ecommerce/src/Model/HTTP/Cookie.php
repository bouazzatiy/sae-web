<?php

namespace Ecommerce\src\Model\HTTP;

class Cookie
{
    public static function contient($cle): bool
    {
        return isset($_COOKIE[$cle]);
    }

    public static function enregistrer(string $cle, mixed $valeur, ?int $dureeExpiration = null): void
    {
        if (is_null($dureeExpiration)) {
            $dureeExpiration = time() + 3600;
        }
        setcookie($cle, json_encode($valeur), $dureeExpiration);
    }

    public static function lire(string $cle): mixed
    {
        return json_decode($_COOKIE[$cle]);
    }
        
    public static function supprimer($cle): void
    {
        unset($_COOKIE[$cle]);
        setcookie ($_COOKIE[$cle], '', 1);
    }    
}