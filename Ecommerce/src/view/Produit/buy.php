<?php

use Ecommerce\src\Model\Repository\ProduitRepository;


$idproduit = intval($_GET['idProduit']);
$produit = (new ProduitRepository())->select($idproduit); 
$nomProduite = $produit->getNom();
$descriptionProduite = $produit->getDescription();
$srcImage= htmlspecialchars($produit->getImage());
echo "  
        
        <div>
        
                <h2>$nomProduite</h2>
                
                <div class='affichageProduit'>
                    
                    <img class=\"image_feuille2\" src=\"image/$srcImage\"alt=\"$nomProduite\">

                    <div class='descriptionProduit'> 
                            <h3> DESCRIPTION </h3> 
                            <p>$descriptionProduite</p>
                            <div>
                                <a class='boutonStyleLBR' href = '?action=add&controller=Produit&idProduit=$idproduit'>Ajouter au panier</a> 
                                <a class='boutonStyleLBR' href = '?controller=Produit'>Retour</a>
                            </div>
                    </div>
                    
                </div>

        </div>       
        
";