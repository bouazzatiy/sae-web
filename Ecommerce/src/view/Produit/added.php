<?php

use Ecommerce\src\Model\Repository\ProduitRepository;

$produit=(new ProduitRepository())->select($idProduit);
$nomProduit=htmlspecialchars($produit->getNom());
echo "
    <div class='feuilleAjoutee'>
    <p><strong>$nomProduit</strong> a bien été ajouté au panier.</p>
    <div class='centreurBouton'>
        <a class='boutonStyleLBR' href='?controller=panier'>Mon panier</a>
        <a class='boutonStyleLBR' href='?controller=produit'>Continuer mes achats</a>
</div>
</div>
";